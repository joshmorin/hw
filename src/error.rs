use std::{fmt, error, result};

pub type Result<T> = result::Result<T, Error>;

/// Error Kind
#[derive(Clone, Copy, PartialEq, Debug)]
pub enum ErrorKind {
    /// An operation is not supported by the underlying implementation
    Unsupported,
    /// An operation failed
    Failure
}

/// Error description type for Hw
#[derive(Clone, Debug)]
pub struct Error {
    kind: ErrorKind,
    desc: String
}

impl Error {
    pub fn unsupported(gerund_phrase: &str) -> Error {
        Error {
            kind: ErrorKind::Unsupported,
            desc: format!("{} is unsupported by the underlying implementation.", gerund_phrase)
        }
    }
    pub fn failure(gerund_phrase: &str, desc: &str) -> Error {
        Error {
            kind: ErrorKind::Failure,
            desc: format!("{} has failed: {}", gerund_phrase, desc)
        }
    }
    pub fn kind(&self) -> ErrorKind {self.kind}
}

impl error::Error for Error {fn description(&self) -> &str {&self.desc}}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> result::Result<(), fmt::Error> {
        use std::io::Write;
        writeln!(f, "{}", self.desc)
    }
}
