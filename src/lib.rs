/*
    Hw: A cross-platform hardware controls API
                     
    Copyright © 2015  Josh Morin <JoshMorin@gmx.com>

    This program is free software: you can redistribute it and/or 
    modify it under the terms of the GNU Lesser General Public 
    License as published by the Free Software Foundation, either 
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with this program. If not, see 
    <http://www.gnu.org/licenses/>.
*/
//!
//! ```
//!
//! use hw;
//!
//!
//! // Sets display brightness to 60%
//! let _ = hw::display_brightness(0.0);
//!
//! // Mutes the systems speaker or connected headphone.
//! let _ = hw::speaker_unmute();
//!
//! // Shutdown via a shutdown signal to the host operating system
//! //let _ = hw::power_shutdown();
//! 
//! ```
//! 
//! Crate level functions (`display_brightness`, `speaker_unmute`, and 
//! `power_shutdown`) are simple wrappers around their `Hw` equivalents.
//!
//!
//! ## ENVIRONMENT VARIABLES ##
//! 
//! 1. `HWLIB_ERROR_LOG`: If set, prints verbose logging of internal events to
//!                       stderr.
//! 2. Additional environment variables may be significant on some platforms. 
//!    refer to the `privilege` and `unprivileged` modules for further details.
//! 
// things to do:
//      keyboard/mouse input (Implement into iterator)
//      screen suspend
//      audio suspend
//      screen subsystem
//      system suspend
//      drop root for cmd? : implement as a shell script 
//                           (simpler than using fork and IPC)
//      
// Implementors' guidelines
// =======================
// 
// 1. Performance is desired but is not a primary concern for this library.
// 2. Avoid storing state. State is kept by the physical hardware or 
//    operating system and should be queried if needed.
// 3. Assume multiple instances used simultaneously from multiple threads 
//    when using ffi or interacting directly with hardware.

extern crate libc;

macro_rules! log {($($arg:tt)*)=>({use std::env; if env::var("HWLIB_ERROR_LOG").is_ok() {
        print!("[HWLIB] {}:{}: ", file!(), line!());println!($($arg)*)}})}


macro_rules! log_n_ret {
    ($gerund_phrase:expr, $($arg:tt)*) => ({
        #[allow(unused_imports)]
        use ::error::{Result, Error};
        
        let error = Error::failure($gerund_phrase, &format!($($arg)*));;
        log!("{}", error);
        return Err(error);
    })
}

mod error;
pub use error::*;

#[cfg(target_os="linux")] mod linux;
#[cfg(target_os="linux")] pub use linux::PlatformHw;

//#[cfg(target_os="???")] mod ???;
//#[cfg(target_os="???")] pub use ???::PlatformHw;







// Deref was avoided to use Hw as an API PlatformHw must adhere to; PlatformHw
// must to implement what Hw needs for the library to compile. What Hw needs is
// the libraries API.
//
// This also allows PlatformHw to offer platform-specific APIs without 
// affecting Hw's cross-platform API.
//
// It's also a good place for API documentation.

/// Hardware Handle
#[derive(Debug)]
pub struct Hw(PlatformHw);
impl Hw {
    /// Return a new hardware handle.
    pub fn new() -> Hw {
        Hw(PlatformHw::new())
    }

    /// Immediate system power off.
    /// 
    /// **Warning:** May cause data loss and other undesirable side-effects associated with
    /// an "Unclean" shutdown. Consider `System::shutdown` if this is 
    /// undesirable.
    pub fn power_off(&mut self) -> Result<()> {self.0.power_off()}
    /// Immediate system reboot.
    /// 
    /// **Warning:** May cause data loss and other undesirable side-effects associated with
    /// an "Unclean" shutdown. Consider `System::restart` if this is 
    /// undesirable.
    pub fn power_reboot(&mut self) -> Result<()> {self.0.power_reboot()}
    /// Sends the shutdown signal for an operating system-assisted power off.
    ///
    /// Exact behavior is system-dependent, and may entail additional user action 
    /// (confirmation). Successful result may not imply successful shutdown 
    /// initiation.
    pub fn power_shutdown(&mut self) -> Result<()> {self.0.power_shutdown()}
    /// Sends the restart signal for an operating system-assisted reboot.
    ///
    /// Exact behavior is system-dependent, and may entail additional user action 
    /// (confirmation). Successful result may not imply successful shutdown 
    /// initiation.
    pub fn power_restart(&mut self) -> Result<()> {self.0.power_restart()}
    /*
        /// Sends the sleep signal for an operating system-assisted reboot.
        /// XXX should use suspend or leave for immediate suspend? Does immediate
        /// suspend even make sense?
        /// 
        /// Exact behavior is system-dependent, and may entail additional user action 
        /// (confirmation). Successful result may not imply successful shutdown 
        /// initiation.
        pub fn sleep() -> Result<()> {try_privileged_then_unprivileged!(System, suspend)}
    */

    /// Sets system audio output volume to a specified level. `level` range 
    /// must be a value either 0.0 (min), 1.0 (max), or any value in between. 
    pub fn speaker_volume(&mut self, level: f64) -> Result<()> {self.0.speaker_volume(level)}
    /// Mutes system audio output
    pub fn speaker_mute(&mut self) -> Result<()> {self.0.speaker_mute()}
    /// Unmutes system audio output
    pub fn speaker_unmute(&mut self) -> Result<()> {self.0.speaker_unmute()}
    
    /// Sets all system video output brightness to a specified intensity. 
    /// `intensity` range must be a value either 0.0 (min), 1.0 (max), 
    /// or any value in between. 
    pub fn display_brightness(&mut self, intensity: f64) -> Result<()> {self.0.display_brightness(intensity)}
    /// Turns off all video output for the system.
    pub fn display_off(&mut self) -> Result<()> {self.0.display_off()}
    /// Turns on all video output for the system.
    pub fn display_on(&mut self) -> Result<()> {self.0.display_on()}
}

// an ergonomic api to elide instating an object with an arity of zero. We 
// rust users must use fancy words to satisfy our complex. 
macro_rules! ergonomic_api {
    ($name: ident) => {
        /// See `Hw`
        pub fn $name() -> Result<()> {Hw::new().$name()}
    };
    ($name: ident, $arg_name: ident, $arg_type: ty) => {
        /// See `Hw`
        pub fn $name($arg_name: $arg_type) -> Result<()> {Hw::new().$name($arg_name)}
    };
}


ergonomic_api!(power_off);
ergonomic_api!(power_reboot);
ergonomic_api!(power_shutdown);
ergonomic_api!(power_restart);
ergonomic_api!(speaker_volume, level, f64);
ergonomic_api!(speaker_mute);
ergonomic_api!(speaker_unmute);
ergonomic_api!(display_brightness, intensity, f64);
ergonomic_api!(display_on);
ergonomic_api!(display_off);


/*
#[cfg(test)] mod events {
    
    struct KeyEvent {
        time:       Option<i64>,
        code:       u32,
        pressed:    bool,
    }

    enum InputEvent {
        Keyboard(K)
    }
    
    
    const EV_KEY              :u32 = 0x01;
    
    const KEY_RESERVED        :u32 = 0;
    const KEY_ESC             :u32 = 1;
    const KEY_1               :u32 = 2;
    const KEY_2               :u32 = 3;
    const KEY_3               :u32 = 4;
    const KEY_4               :u32 = 5;
    const KEY_5               :u32 = 6;
    const KEY_6               :u32 = 7;
    const KEY_7               :u32 = 8;
    const KEY_8               :u32 = 9;
    const KEY_9               :u32 = 10;
    const KEY_0               :u32 = 11;
    const KEY_MINUS           :u32 = 12;
    const KEY_EQUAL           :u32 = 13;
    const KEY_BACKSPACE       :u32 = 14;
    const KEY_TAB             :u32 = 15;
    const KEY_Q               :u32 = 16;
    const KEY_W               :u32 = 17;
    const KEY_E               :u32 = 18;
    const KEY_R               :u32 = 19;
    const KEY_T               :u32 = 20;
    const KEY_Y               :u32 = 21;
    const KEY_U               :u32 = 22;
    const KEY_I               :u32 = 23;
    const KEY_O               :u32 = 24;
    const KEY_P               :u32 = 25;
    const KEY_LEFTBRACE       :u32 = 26;
    const KEY_RIGHTBRACE      :u32 = 27;
    const KEY_ENTER           :u32 = 28;
    const KEY_LEFTCTRL        :u32 = 29;
    const KEY_A               :u32 = 30;
    const KEY_S               :u32 = 31;
    const KEY_D               :u32 = 32;
    const KEY_F               :u32 = 33;
    const KEY_G               :u32 = 34;
    const KEY_H               :u32 = 35;
    const KEY_J               :u32 = 36;
    const KEY_K               :u32 = 37;
    const KEY_L               :u32 = 38;
    const KEY_SEMICOLON       :u32 = 39;
    const KEY_APOSTROPHE      :u32 = 40;
    const KEY_GRAVE           :u32 = 41;
    const KEY_LEFTSHIFT       :u32 = 42;
    const KEY_BACKSLASH       :u32 = 43;
    const KEY_Z               :u32 = 44;
    const KEY_X               :u32 = 45;
    const KEY_C               :u32 = 46;
    const KEY_V               :u32 = 47;
    const KEY_B               :u32 = 48;
    const KEY_N               :u32 = 49;
    const KEY_M               :u32 = 50;
    const KEY_COMMA           :u32 = 51;
    const KEY_DOT             :u32 = 52;
    const KEY_SLASH           :u32 = 53;
    const KEY_RIGHTSHIFT      :u32 = 54;
    const KEY_KPASTERISK      :u32 = 55;
    const KEY_LEFTALT         :u32 = 56;
    const KEY_SPACE           :u32 = 57;
    const KEY_CAPSLOCK        :u32 = 58;
    const KEY_F1              :u32 = 59;
    const KEY_F2              :u32 = 60;
    const KEY_F3              :u32 = 61;
    const KEY_F4              :u32 = 62;
    const KEY_F5              :u32 = 63;
    const KEY_F6              :u32 = 64;
    const KEY_F7              :u32 = 65;
    const KEY_F8              :u32 = 66;
    const KEY_F9              :u32 = 67;
    const KEY_F10             :u32 = 68;
    const KEY_NUMLOCK         :u32 = 69;
    const KEY_SCROLLLOCK      :u32 = 70;
    const KEY_KP7             :u32 = 71;
    const KEY_KP8             :u32 = 72;
    const KEY_KP9             :u32 = 73;
    const KEY_KPMINUS         :u32 = 74;
    const KEY_KP4             :u32 = 75;
    const KEY_KP5             :u32 = 76;
    const KEY_KP6             :u32 = 77;
    const KEY_KPPLUS          :u32 = 78;
    const KEY_KP1             :u32 = 79;
    const KEY_KP2             :u32 = 80;
    const KEY_KP3             :u32 = 81;
    const KEY_KP0             :u32 = 82;
    const KEY_KPDOT           :u32 = 83;

    pub fn events() {
        use libc::types::os::common::posix01::timeval;
        use libc::types::os::arch::c95::{c_ushort, c_int};
        use std::fs::File;
        use std::io::Read;
        use std::slice;
        use std::mem;
        
        #[repr(C)]
        struct InputEvent {
	        time    : timeval,
	        ty      : c_ushort,
	        code    : c_ushort,
	        value   : c_int
        }
        
        let mut f = File::open("/dev/input/event3")
                    .unwrap_or_else(|e| panic!("couldn't open it: {}", e));
        
        let mut input_event: InputEvent = unsafe {mem::uninitialized()};
        
        println!("listening...");
        while f.read(
            unsafe{
                slice::from_raw_parts_mut(
                    &mut input_event as *mut InputEvent as *mut u8, 
                    mem::size_of::<InputEvent>()
                )
            }
        ).is_ok() {
        
            println!("{:x} {:x} {:x}", input_event.ty, input_event.code, input_event.value);
        }
    }
}
*/

#[test]
fn it_works() {
    //use ::events;
    //events::events();
    
    use std::{env, thread};
    macro_rules! test_gradient {
        ($hw: expr, $func: ident) => ({
            println!("testing gradient for {}...", stringify!($func));
            let resolution = 200.0;
            //let mut g = Hw::new().$f().unwrap_or_else(|e| panic!("{}", e));
            let time = 0.5; //seconds
            for i in 0..(resolution as u32 +1) { 
                if $hw.$func(((i as f64-(resolution/2.0)).abs())/100.0).is_err() {
                    break;
                }
                thread::sleep_ms((time*1000.0/resolution) as u32);
            }
        })
    }
    
    macro_rules! test_toggle {
        ($hw: expr, $off: ident, $on: ident) => ({
            println!("toggling test for {}/{}...", stringify!($on), stringify!($off));
            //let mut g = Hw::new().$f().unwrap_or_else(|e| panic!("{}", e));
            loop {
                println!("{}", stringify!($off));
                if $hw.$off().is_err() {break};
                thread::sleep_ms(1000);
                println!("{}", stringify!($on));
                if $hw.$on().is_err() {break};
                break;
            }
        })
    }
    
    env::set_var("HWLIB_ERROR_LOG", "Y");
    
    let mut hw = Hw::new();
    test_gradient!(hw, display_brightness);
    test_gradient!(hw, speaker_volume);
    test_toggle!(hw, display_off, display_on);
    test_toggle!(hw, speaker_mute, speaker_unmute);
    
    
    hw.display_brightness(1.0).unwrap();
    hw.speaker_volume(0.5).unwrap();
    
    /*
    println!("WARNING: Emergency power off in 2 secconds..");
    thread::sleep_ms(2000);
    Hw::new().power_off();
    println!("Couldn't reboot");
    */
    
}
