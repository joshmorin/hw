// https://www.kernel.org/doc/Documentation/input/input.txt
// https://www.kernel.org/doc/Documentation/input/event-codes.txt
// file:///usr/include/linux/input.h

mod io;
mod toolbox;


use ::error::Result;

macro_rules! try_privileged_then_unprivileged {
    ($s: expr, $func: ident) => ({
        match privileged::$func($s) {
            Ok(o) => Ok(o),
            _ => {
                unprivileged::$func($s)
            }
        }
    });
    ($s: expr, $func: ident, $arg1: expr) => ({
        match privileged::$func($s, $arg1) {
            Ok(o) => Ok(o),
            _ => unprivileged::$func($s, $arg1)
        }
    });
}

macro_rules! define_method {
    ($name: ident) => {
        pub fn $name(&mut self) -> Result<()> {try_privileged_then_unprivileged!(self, $name)}
    };
}

/// (Linux) Platform-specific Hardware Handle
#[derive(Debug)]
pub struct PlatformHw(());
impl PlatformHw {
    pub fn new() -> PlatformHw {
        PlatformHw(())
    }
    pub fn power_off(&mut self) -> Result<()> {
        use ::linux::toolbox::sysrq::SysRq;
        match SysRq::power_off() {
            Ok(_) => return Ok(()),
            Err(e) => log_n_ret!("power_off via /proc/sysrq", "{}", e)
        }
    }
    pub fn power_reboot(&mut self) -> Result<()> {
        use ::linux::toolbox::sysrq::SysRq;
        match SysRq::reboot() {
            Ok(_) => return Ok(()),
            Err(e) => log_n_ret!("power_reboot via /proc/sysrq", "{}", e)
        }
    }
    pub fn power_shutdown(&mut self) -> Result<()> {
        use ::linux::toolbox::shutdown::Shutdown;
        match Shutdown::shutdown() {
            Ok(_) => return Ok(()),
            Err(e) => log_n_ret!("power_shutdown via shutdown binary", "{}", e)
        }
    }
    pub fn power_restart(&mut self) -> Result<()> {
        use ::linux::toolbox::shutdown::Shutdown;
        match Shutdown::restart() {
            Ok(_) => return Ok(()),
            Err(e) => log_n_ret!("power_restart via shutdown binary", "{}", e)
        }
    }
    
    pub fn speaker_volume(&mut self, level: f64) -> Result<()> {
        use ::linux::toolbox::pactrl::PaCtrl;
        match PaCtrl::set_volume(level) {
            Ok(_) => return Ok(()),
            Err(e) => log_n_ret!("speaker_volume via pactrl binary", "{}", e)
        }
    }
    pub fn speaker_mute(&mut self) -> Result<()> {
        use ::linux::toolbox::pactrl::PaCtrl;
        match PaCtrl::mute() {
            Ok(_) => return Ok(()),
            Err(e) => log_n_ret!("speaker_mute via pactrl binary", "{}", e)
        }
    }
    pub fn speaker_unmute(&mut self) -> Result<()> {
        use ::linux::toolbox::pactrl::PaCtrl;
        match PaCtrl::unmute() {
            Ok(_) => return Ok(()),
            Err(e) => log_n_ret!("speaker_unmute via pactrl binary", "{}", e)
        }
    }
    pub fn display_brightness(&mut self, intensity: f64) -> Result<()> {
        use ::linux::toolbox::xbacklight::Xbacklight;
        match Xbacklight::set_level(intensity) {
            Ok(_) => return Ok(()),
            Err(e) => log_n_ret!("display_brightness via xbacklight binary", "{}", e)
        }
    }
    pub fn display_on(&mut self) -> Result<()> {
        use ::linux::toolbox::vbetool::VbeTool;
        match VbeTool::on() {
            Ok(_) => return Ok(()),
            Err(e) => log_n_ret!("display_on via vbetool binary", "{}", e)
        }
    }
    pub fn display_off(&mut self) -> Result<()> {
        use ::linux::toolbox::vbetool::VbeTool;
        match VbeTool::off() {
            Ok(_) => return Ok(()),
            Err(e) => log_n_ret!("display_off via vbetool binary", "{}", e)
        }
    }
}
