use std::io;
use ::linux::toolbox::execute_command;


pub struct VbeTool;
impl VbeTool {
    pub fn off() -> io::Result<()> {
        match exec!("vbetool", "dpms", "off") {
            Ok(_) => Ok(()),
            Err(e) => Err(e)
        }
    }
    pub fn on() -> io::Result<()> {
        match exec!("vbetool", "dpms", "on") {
            Ok(_) => Ok(()),
            Err(e) => Err(e)
        }
    }
}
