use std::{io, process};
use std::path::Path;
use std::ffi::OsStr;

macro_rules! exec {
    ($($arg:expr ),*) => ({
        use ::linux::toolbox;
        toolbox::execute_command(&[  $(($arg).as_ref() ,)* ])
    })
}
macro_rules! exec_unprev {
    ($($arg:expr ),*) => ({
        use ::linux::toolbox;
        toolbox::execute_unprevlaged_command(&[  $(($arg).as_ref() ,)* ])
    })
}


pub mod pactrl;
pub mod xbacklight;
pub mod vbetool;
pub mod shutdown;
pub mod sysrq;
pub mod sysfs_backlight;


pub fn strip_newline(s: &str) -> &str {
    let mut l = s.len();
    while l != 0 {
        if &s[l-1..] == "\n" {l-=1}
        else {break}
    }
    &s[..l]
}
pub fn write_line<P: AsRef<Path>>(path: P, s: &str) -> io::Result<()> {
    use std::fs;
    use std::io::Write;
    
    try!(try!(fs::File::create(path)).write_all(s.as_bytes()));
    Ok(())
}

pub fn read_file<P: AsRef<Path>>(path: P) -> io::Result<String> {
    use std::fs;
    use std::io::Read;
    let mut s = String::new();
    try!(try!(fs::File::open(path)).read_to_string(&mut s));
    Ok(s)
}


pub fn execute_unprevlaged_command(cmd: &[&OsStr]) -> io::Result<process::Output> {
    use std::process::Command;
    use std::io::{Error, ErrorKind};
    use std::str;
    
    let mut command = Command::new(&cmd[0]);
    command.args(&cmd[1..]);   
    
    match command.output() {
        Ok(output) => {
            if output.status.success() {
                Ok(output)
            }
            else {
                Err(Error::new(ErrorKind::Other, format!("External command failed to execute: {:?}: {}", 
                    cmd, strip_newline(str::from_utf8(&output.stderr[..]).unwrap_or("")))))
            }
        },
        Err(e) => Err(Error::new(ErrorKind::Other, format!("External command failed to execute: {:?}: {}", cmd, strip_newline(&e.to_string()))))
    }
}
pub fn execute_command(cmd: &[&OsStr]) -> io::Result<process::Output> {
    use std::process::Command;
    use std::io::{Error, ErrorKind};
    use std::str;
    
    let mut command = Command::new(&cmd[0]);
    command.args(&cmd[1..]);   
    
    match command.output() {
        Ok(output) => {
            if output.status.success() {
                Ok(output)
            }
            else {
                Err(Error::new(ErrorKind::Other, format!("External command failed to execute: {:?}: {}", 
                    cmd, strip_newline(str::from_utf8(&output.stderr[..]).unwrap_or("")))))
            }
        },
        Err(e) => Err(Error::new(ErrorKind::Other, format!("External command failed to execute: {:?}: {}", cmd, strip_newline(&e.to_string()))))
    }
}

pub fn clip_percentage(f: f64) -> f64 {
    if f > 1.0 {1.0}
    else if f < 0.0 {0.0}
    else {f}
}
