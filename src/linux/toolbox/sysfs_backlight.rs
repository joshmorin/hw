/*
// https://www.kernel.org/doc/Documentation/ABI/stable/sysfs-class-backlight
use std::io;
use ::linux::toolbox::clip_percentage;

pub struct SysfsBacklight;
impl SysfsBacklight {
    pub fn set_level(intensity: f64) -> io::Result<()> {
        let intensity = clip_percentage(intensity);
        Ok(())
    }
    pub fn get_level() -> io::Result<f64> {
        Ok(0.0)
    }
}



pub fn read_files(path: &str) -> io::Result<vec![String]> {
    use std::fs;
    use std::io::Read;
    let mut out = vec![];
    let mut s = String::new();
    
    let path_segments = path.splitn(2, '*');
    let head = path_segments.next();
    let tail = path_segments.next();
    
    match (head, tail) {
        (Some(head), Some(tail)) => {
            
        },
        (Some(head), None) => {
        
        },
        _ => Ok(vec![])
    }
    
    
    try!(try!(fs::File::open(path)).read_to_string(&mut s));
    
    Ok(v)
}






// Control the brightness of a backlight.
// 
// `devices` is a set of devices to attempt to set the brights for. 
// `intensity` is the desired intensity ranging from 0.0 to 1.0.
// 
// Passing `MultiChoice::All` to this function implies that all devices
// should be updated. `MultiChoice::Default` implies `MultiChoice::All` for
// this function.
//
// If the `HWLIB_BRIGHTNESS_DEVICE` global variable is set, the device
// specified will override the device defined in `devices`.
//     
// This function succeeds if any device has been successfully updated.
// 
fn linux_sysfs_class_backlight_intensity(devices: Option<Vec<String>>, intensity: f64) -> Result<()> {
    //:‎ ‫يشتكي brightness إذا ختم السطر بـnewline. لا أدري إن كان هذا جزء من الـABI أو خلل.
    use std::{fs, env, io};
    use std::io::{Read, Write};
    use std::io::ErrorKind::Other as o;
    use std::num::ParseIntError;
    use std::error::Error;
    
    let mut intensity = intensity;
    let mut status = false;
    
    
    // None emplys that all devices are selected.
    let device_list = if let Ok(device) = env::var("HWLIB_BRIGHTNESS_DEVICE") {
        log!("Found that HWLIB_BRIGHTNESS_DEVICE='{}'. Ignoring parameter devices.", device); 
        Some(vec![device])
    } 
    else {devices};
    
    
    
    
    let dir_list = match fs::read_dir("/sys/class/backlight") {
        Ok(dir_list) => dir_list,
        Err(e) => {
            log_n_ret!("Setting display brightness", "Could not list files in '/sys/class/backlight': {}", e);
        }
    };
    
    
    for dir_entry in dir_list {
        let dir_entry = match dir_entry {
            Ok(dir_entry) => dir_entry,
            Err(e) => {
                log!("Could not read a directory entry: {}", e); 
                continue;
            }
        };
        
        
        let path = dir_entry.path();
        if let (&Some(ref list), &Some(ref current_device)) = (&device_list, &path.file_name()) {
            if !list.iter().any(|device_name| &device_name[..] == *current_device) {
                continue
            }
        }
        
        let r = (|| -> io::Result<()> {
            let mut max: String = String::new();
            let max_path = path.clone().join("max_brightness");
            
            try!(try!(fs::File::open(&max_path)).read_to_string(&mut max));
            
            let max: &str = try!(max.lines().next()
                                 .ok_or(io::Error::new(o, 
                                    format!("Expected at least one line in {}", 
                                        max_path.display()))));
                            
            let max: isize = try!(max.parse()
                                  .map_err(|e: ParseIntError| io::Error::new(
                                    o, e.description())));
            
            if max <= 0 {
                return Err(io::Error::new(o, "Invalid max_brightness found"))
            }
            
            let new_brightness = (max as f64 * intensity) as i64;
            let brightness_path = path.clone().join("brightness");
            try!(write!(try!(fs::OpenOptions::new().truncate(true)
                               .write(true).open(&brightness_path)), 
                            "{}", new_brightness));
            Ok(())
        })();
        
        if let Err(e) = r {
            log!("{}", e);
        }
        else {
            status = true;
        }
    }
    
    if status {Ok(())}
    else {
        log_n_ret!("Setting display brightness",
            concat!("None of the selected devices were updated successfully. ",
            "Selected devices are: {}"), if let Some(dl) = device_list {
            format!("{:?}", dl) } else {format!("(All devices selected)")});
    }
}*/
