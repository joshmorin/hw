use std::io;

pub struct SysRq;
impl SysRq {
    pub fn power_off() -> io::Result<()> {
        use ::linux::toolbox::write_line;
        write_line("/proc/sysrq-trigger", "o")
    }
    pub fn reboot() -> io::Result<()> {
        use ::linux::toolbox::write_line;
        write_line("/proc/sysrq-trigger", "r")
    }
}
