use ::linux::toolbox::clip_percentage;
use std::io;

pub struct PaCtrl;
impl PaCtrl {
    pub fn set_volume(level: f64) -> io::Result<()> {
        let level = clip_percentage(level);
        match exec_unprev!("pactl", "set-sink-volume", "@DEFAULT_SINK@", format!("{}%", (level*100.0) as usize)) {
            Ok(_) => Ok(()),
            Err(e) => Err(e)
        }
    }
    pub fn mute() -> io::Result<()> {
        match exec_unprev!("pactl", "set-sink-mute", "@DEFAULT_SINK@", "1") {
            Ok(_) => Ok(()),
            Err(e) => Err(e)
        }
    }
    pub fn unmute() -> io::Result<()> {
        match exec_unprev!("pactl", "set-sink-mute", "@DEFAULT_SINK@", "0") {
            Ok(_) => Ok(()),
            Err(e) => Err(e)
        }
    }
}
