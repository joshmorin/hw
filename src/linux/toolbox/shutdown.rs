use std::io;
use ::linux::toolbox::execute_command;


pub struct Shutdown;
impl Shutdown {
    pub fn shutdown() -> io::Result<()> {
        match exec!("shutdown", "-P", "0") {
            Ok(_) => Ok(()),
            Err(e) => Err(e)
        }
    }
    pub fn restart() -> io::Result<()> {
        match exec!("shutdown", "-r", "0") {
            Ok(_) => Ok(()),
            Err(e) => Err(e)
        }
    }
}
