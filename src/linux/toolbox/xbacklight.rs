use std::io;

pub struct Xbacklight;
impl Xbacklight {
    pub fn set_level(intensity :f64) -> io::Result<()> {
        use ::linux::toolbox::clip_percentage;
        let intensity = clip_percentage(intensity);
        match exec_unprev!("xbacklight", "-steps", "1", "-time", "0", "-set", format!("{}", (intensity*100.0) as usize)) {
            Ok(_) => Ok(()),
            Err(e) => Err(e)
        }
    }
    pub fn get_level() -> io::Result<f64> {
        use std::str;
        use ::linux::toolbox::strip_newline;
        
        match exec_unprev!("xbacklight", "-get") {
            Ok(o) => {
                let s = match str::from_utf8(&o.stdout[..]) {
                    Ok(s) => s, _ => return Err(io::Error::new(
                        io::ErrorKind::Other, format!(
                            "xbacklight returned something not utf-8: {:?}", 
                            &o.stdout[..])))
                };
                let s = strip_newline(s);
                let f:f64 = match s.parse() {
                    Ok(f) => f, _ => return Err(io::Error::new(
                        io::ErrorKind::Other, format!(
                            "xbacklight returned something that is not a number: '{}'", 
                            s)))
                };
                Ok(f)
            },
            Err(e) => Err(e)        
        }
    }
}
