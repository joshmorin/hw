The Hw library is available under the terms of the LGPL license as 
described bellow:

Copyright © 2015  Josh Morin <JoshMorin@gmx.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

Full text of the LGPL can be found in the root directory of this project 
under the name 'lgpl.txt'.

Full text of the GPL  can be found in the root directory of this project 
under the name 'gpl.txt'.




