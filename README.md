Hw: A cross-platform hardware controls API
==========================================

Hw premise and scope: If an operation is usually associated with a hardware 
control or makes sense as a hardware control, and that operation can be 
initiated by software, then it belongs in this library.

Brightness and volume adjustments are functions that belong in this library. 
Playing audio and killing processes are not.


    use hw;


    // Sets display brightness to 60%
    let _ = hw::display_brightness(0.6);

    // Mutes the systems speaker or connected headphone.
    let _ = hw::speaker_unmute();

    // Shutdown via a shutdown signal to the host operating system
    let _ = hw::power_shutdown();
    

# Library Principles #

1. Limit functionality to what makes sense as a hardware control.
2. Stay as close to expected hardware behavior as possible.
3. Adapt to whatever prevlages the library is invoked under.


# Current Library State and Known Issues #

1. Linux is the only system currently supported. Support for Android, 
   Windows, and other systems is planned. Contributions welcome.
2. Currently using command-line programs to achieve desired behavior for 
   Linux. Libraries might replace programs in future versions if time
   and resources permit.


# Assumptions #

1. A single-seat system: Accommodating multi-seat systems is not required.


# Building #
This project makes use of Rust's standard build system. If not 
available at crates.io (Currently, it isn't), you can add a git 
dependency to your project:

    [dependencies.hw]
    name = "hw"
    git = "https://joshmorin@bitbucket.org/joshmorin/hw.git"


# Documentation #

Online [API](https://joshmorin.github.io/documentation/hw) reference. 

To insure that the documentation matches the library version you are using, 
consider generating the documentation using cargo.


# Bugs #

Please submit an issue to the projects issue tracker at: 
https://bitbucket.org/joshmorin/hw/issues/new

**ANONYMOUS SUBMISSIONS ARE ALLOWED**. Alternatively, You may also 
email me at JoshMorin@gmx.com (Please prefix the subject with 
"BUG").



# Contribution #

For contributing to this library, please submit an issue before starting. 
You may also email me at JoshMorin@gmx.com. *I can not guarantee accepting 
a patch if an accompanying bug report is not submitted before hand.* 

    
# Copyright #

Copyright © 2015  Josh Morin <JoshMorin@gmx.com>

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
